@echo off

REM
REM This bat file is used to protect a PDF file with a password
REM
REM Tested on Windows 11
REM 



setlocal enabledelayedexpansion

set count=0
set "choice_options="

echo.
for %%A in (*.pdf) do (
    
	REM Increment %count% here so that it doesn't get incremented later
    set /a count+=1

    REM Add the file name to the options array
    set "options[!count!]=%%A"

    REM Add the new option to the list of existing options
    set choice_options=!choice_options!!count!
)

for /L %%A in (1,1,!count!) do echo [%%A]. !options[%%A]!
set /p filechoice="Enter a file to extract pages from: "
set /p firstPage="Enter page number of the first page: "
set /p lastPage="Enter page number of the last page: "

echo Running !options[%filechoice%]!...

REM extract filename without extension as preparation for the new filename
set filename=!options[%filechoice%]!
FOR /F "delims=" %%i IN ("%filename%") DO (
set shortname=%%~ni
)

REM now run the extraction
"%~dp0\qpdf-10.6.3\bin\qpdf.exe" --collate --empty  --pages "!options[%filechoice%]!" %firstpage%-%lastpage% -- "%shortname%"_S%firstPage%-%lastpage%.pdf
