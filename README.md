# General

This commands are primarly base on the project <a href="https://github.com/qpdf/qpdf" target="_blank">QPDF</a>. 
Currently they are only windows bat files which creates a dynamic menu in the windows console to choose the pdf to protect with a password
or to extract pages from. That's all....

# Installation

The easiest way to install this commands are download the repository as archive (zip or tar.gz) and unpack it.
Copy the folder to e.g. C:/programs/portable apps/w2pdf.
Put this path to the environment PATH variable and that's it.
To run the commands go to a directory where your PDF is stored and open a CMD in this directory.
Enter "setPDFpw" or "extractPagesOfPDF" und follow the instructions on the screen.

# Commands

## setPDFpw

Protect a PDF file with a password


## extractPagesOfPDF

Extracts some pages from a PDF file.


# License

